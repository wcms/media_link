/**
 * @file
 */

(function ($) {
  $(function () {
    $('audio.mediaelement').mediaelementplayer({audioWidth: $('.mediaelement').closest('.field').width()});
  });
})(jQuery);
